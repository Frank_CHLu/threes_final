#include <cstdlib>
#include <random>
#include <vector>
#include <chrono>
#include <fstream>
#include <algorithm>
#include "board.h"
#include "move.h"
#define forstate for(int i = 0 ; i < 4 ; i++) for(int j = 0 ; j < 4 ; j++) for(int k = 0 ; k < 4 ; k++)
typedef unsigned long long ULL;
std::default_random_engine engine;
std::uniform_int_distribution <int> dis(0,20);
std::vector<Move> record;
extern Index *Tuple[4][4][4];
extern ULL num_Tuple[4][4][4];
void Computer(Board &board, const int dir, int &next_tile, Move &p)
{
    static int next[4][4] = {{12, 13, 14, 15}, {0, 4, 8, 12}, {0, 1, 2, 3}, {3, 7, 11, 15}};
    static int bag[12] = {1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3}, bag_index;
    static int location[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    if(dir == 4)
    {
        board = Board();
        bag_index = 0;
        shuffle(bag, bag + 12, engine);
        shuffle(location, location + 16, engine);
        while(bag_index < 9)
        {
            board.take_place(location[bag_index], bag[bag_index]);
            record.push_back(Move(location[bag_index], bag[bag_index]));
            bag_index++;
        }
        next_tile = bag[9];
        return ;
    }
    else
    {
        shuffle(next[dir], next[dir] + 4, engine);
        for(int i = 0 ; i < 4 ; i++) if(board.take_place(next[dir][i], next_tile)) {p = Move(next[dir][i], next_tile); break;}
        if(next_tile < 4) bag_index++;
        if(bag_index == 12)
        {
            bag_index = 0;
            shuffle(bag, bag + 12, engine);
        }
        next_tile = bag[bag_index];
        if(board.maxtile() >= 7 && !dis(engine)) 
        {
        	std::uniform_int_distribution <int> tile(4, board.maxtile() - 3);
        	next_tile = tile(engine);
        }
    }
}
void Player(Board &board, const int next_tile, Move &p)
{
    int dir = -1;
    float maxvalue;
    #pragma omp parallel for num_threads(4)
    for(int i = 0 ; i < 4 ; i++)
    {
        Board tmp = board;
        if(tmp.slide(i) != -1)
        {
            float res = tmp.get_tuple(NULL, next_tile > 3 ? 3 : next_tile - 1, i);
            #pragma omp critical
    	    {	
                if(dir == -1 || res > maxvalue)
                {
                    dir = i;
                    maxvalue = res;
                }
            }
        }
    }
    if(dir == -1) {p = Move(); return ;}
    board.slide(dir);
    p = Move(dir);
}
void input()
{
    unsigned index;
    ULL cnt = 0, num;
    float value;
    std::ifstream fin("data", std::ios::binary);
    forstate
    {
        fin.read((char*)&num, sizeof(ULL));
        cnt += num;
        num_Tuple[i][j][k] = num;
        Tuple[i][j][k] = new Index[num];
        while(num--)
        {
            fin.read((char*)&index, sizeof(unsigned));
            fin.read((char*)&value, sizeof(float));
        	Tuple[i][j][k][num].index = index;
        	Tuple[i][j][k][num].value = value;
        }
    }
	fin.close();
	forstate std::sort(Tuple[i][j][k], Tuple[i][j][k] + num_Tuple[i][j][k]);
	printf("The number of Tuple : %llu\n", cnt);
}
time_t get_time()
{
    auto now = std::chrono::system_clock::now().time_since_epoch();
    return std::chrono::duration_cast<std::chrono::milliseconds>(now).count();
}
int main(int n, char **argv)
{
    //std::ios_base::sync_with_stdio(0);
    //std::cin.tie(0);
    int num_game = atoi(argv[1]), next_tile, dir;
    engine.seed(time(NULL));
    std::printf("Start Reading\n");
    input();
    std::printf("Reading Complete.\n");
    Board board;
    Move p;
    time_t time_start, time_end, player = 0, envir = 0, tmp;
    ULL player_step = 0, envir_step = 0;
    std::printf("Start Playing.\n");
    std::ofstream fout("result.txt");
    for(int i = 0 ; i < num_game ; i++)
    {
        record.clear();
        tmp = get_time();
        Computer(board, 4, next_tile, p);
        envir += get_time() - tmp;
        envir_step += 9;
        time_start = get_time();
        while(true)
        {
            tmp = get_time();
            Player(board, next_tile, p);
            player += get_time() - tmp;
            player_step++;
            if(p.slide() == -1) break;
            record.push_back(p);
            tmp = get_time();
            Computer(board, p.slide(), next_tile, p);
            envir += get_time() - tmp;
            envir_step ++;
            record.push_back(p);
        }
        time_end = get_time();
        fout << "0516310:N-Tuple@" << time_start << "|";
        for(int j = 0 ; j < record.size() ; j++) fout << record[j];
        fout << "|N-Tuple@" << time_end << "\n";
    }
    printf("Complete playing.\nPlayer speed: %llu\nEnvir speed: %llu\n", player_step * 1000 / player, envir_step * 1000 / envir);
    return 0;
}
