#include <bits/stdc++.h>
#include <omp.h>
#include "board.h"
#define forstate for(int i = 0 ; i < 4 ; i++) for(int j = 0 ; j < 4 ; j++) for(int k = 0 ; k < 4 ; k++)
using namespace std;
typedef std::unordered_map<unsigned, float> State;
typedef unsigned long long ULL;
State state[4][4][4];
default_random_engine engine;
uniform_int_distribution <int> dis(0,20);
extern Index *Tuple[4][4][4];
extern ULL num_Tuple[4][4][4];
float Computer(Board &board, const int dir, const int next_tile);
float Player(Board &board, const int next_tile)
{
	int dir = -1, reward;
	float maxvalue;
    #pragma omp parallel for num_threads(4)
	for(int i = 0 ; i < 4 ; i++)
	{
		Board tmp = board;
		if(tmp.slide(i) != -1)
		{
			float res = tmp.get_tuple(state[next_tile > 3 ? 3 : next_tile - 1], next_tile > 3 ? 3 : next_tile - 1, i);
			#pragma omp critical
            {   
                if(dir == -1 || res > maxvalue)
                {
                    dir = i;
                    maxvalue = res;
                }
            }
		}
	}
	if(dir == -1) return 0;
	reward = board.slide(dir);
	Board tmp = board;
	float res = Computer(board, dir, next_tile);
	return reward + tmp.tuple_modify(state[next_tile > 3 ? 3 : next_tile - 1], (res - maxvalue) / 640, next_tile > 3 ? 3 : next_tile - 1, dir);;
}
float Computer(Board &board, const int dir, int next_tile)
{
    static int bag[12] = {1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3}, bag_index;
    static int location[16] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
    static int next[4][4] = {{12, 13, 14, 15}, {0, 4, 8, 12}, {0, 1, 2, 3}, {3, 7, 11, 15}};
    if(dir == 4)
    {
        bag_index = 0;
        shuffle(bag, bag + 12, engine);
        shuffle(location, location + 16, engine);
        while(bag_index++ < 9) board.take_place(location[bag_index - 1], bag[bag_index - 1]);
        return Player(board, bag[bag_index]);
    }
    else
    {
        shuffle(next[dir], next[dir] + 4, engine);
        for(int i = 0 ; i < 4 ; i++) if(board.take_place(next[dir][i], next_tile)) break;
        if(next_tile < 4) bag_index++;
        if(bag_index == 12)
        {
            bag_index = 0;
            shuffle(bag, bag + 12, engine);
        }
        next_tile = bag[bag_index];
        if(board.maxtile() >= 7 && !dis(engine))
        {
            uniform_int_distribution <int> tile(4, board.maxtile()-3);
            next_tile = tile(engine);
        }
        return Player(board, next_tile);
    }
}
void initialize()
{
    engine.seed(time(NULL));
    forstate state[i][j][k].clear();
}
void input()
{
    unsigned index;
    ULL cnt = 0, num;
    float value;
    ifstream fin("data", std::ios::binary);
    forstate
    {
        fin.read((char*)&num, sizeof(ULL));
        cnt += num;
        num_Tuple[i][j][k] = num;
        Tuple[i][j][k] = new Index[num];
        while(num--)
        {
            fin.read((char*)&index, sizeof(unsigned));
            fin.read((char*)&value, sizeof(float));
            Tuple[i][j][k][num].index = index;
            Tuple[i][j][k][num].value = value;
        }
    }
    fin.close();
    forstate sort(Tuple[i][j][k], Tuple[i][j][k] + num_Tuple[i][j][k]);
    printf("The number of Tuple : %llu\n", cnt);
}
int main(int n, char **argv)
{
    Board board;
    State::iterator it;
    printf("Database Preparing...\n");
    initialize();
    input();
    printf("Database ready.\n");
    int num_game = atoi(argv[1]);
    printf("Start learning...\n");
    while(num_game--)
    {
	    if(num_game % 10000 == 0) printf("%d\n", num_game);
	    board = Board();
	    Computer(board, 4, 0);
    }
    printf("Complete learning.\nStart writing...\n");
	ULL num = 0, data_size;
    ofstream fout("data", ios::binary);
    forstate
    {
        data_size = state[i][j][k].size() + num_Tuple[i][j][k];
        fout.write((char*)&data_size, sizeof(ULL));
        it = state[i][j][k].begin();
		num += data_size;
        while(it != state[i][j][k].end())
        {
            fout.write((char*)&(it->first), sizeof(unsigned));
            fout.write((char*)&(it->second), sizeof(float));
            it++;
        }
        for(int l = 0 ; l < num_Tuple[i][j][k] ; l++)
        {
            fout.write((char*)&Tuple[i][j][k][l].index, sizeof(unsigned));
            fout.write((char*)&Tuple[i][j][k][l].value, sizeof(float));
        }
    }
	printf("%llu\n", num);
    printf("Complete writing.\n");
    return 0;
}
