learn:
	g++ -o learn -O3 -std=c++11 -fopenmp learning.cpp
Threes:
	g++ -o Threes -O3 -std=c++11 -fopenmp Threes.cpp
clean:
	rm learn
	rm Threes

